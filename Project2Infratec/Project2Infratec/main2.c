// ISIS-1304 - FUNDAMENTOS DE INFRAESTRUCTURA TECNOLOGICA - PROYECTO 2 - 201910
// EL GRUPO DEBE SER DESARROLLADO EN GRUPOS DE A 3 PERSONAS MAXIMO
// DESARROLLADO POR:
// Andres Felipe Orozco Gonzalez - 201730058 (af.orozcog@uniandes.edu.co)
// Juan Andres Avelino Ospina - 201812676  (ja.avelino@uniandes.edu.co)
// Nicolas Andres Tobo Urrutia - 201817465  (na.tobo@uniandes.edu.co)

#define _CRT_SECURE_NO_DEPRECATE 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>

// La representacion de la imagen
typedef struct img
{
	int ancho;
	int alto;
	unsigned char *informacion;
} Imagen;


// Función que carga el bmp en la estructura Imagen
void cargarBMP24(Imagen * imagen, char * nomArchivoEntrada);

// Función que guarda el contenido de la estructura imagen en un archivo binario
void guardarBMP24(Imagen * imagen, char * nomArchivoSalida);

//Función que inserta un mensaje en la imagen usando n bits por Byte
void insertarMensaje(Imagen * img, unsigned char mensaje[], int n);

//Función que lee un mensaje de una imagen dando la longitud del mensaje y el numero de bits por byte usados
void leerMensaje(Imagen * img, unsigned char msg[], int l, int n);

//Función que saca n bits de una secuencia de caracteres a partir de una posición dada
unsigned char sacarNbits(unsigned char secuencia[], int bitpos, int n);

// Programa principal
// NO MODIFICAR
int main(int argc, char* argv[]) {

	Imagen *img = (Imagen *)malloc(sizeof(Imagen));
	char msg[10000] = "";
	char op, temp;
	int num, l, n;
	char nomArch[256] = "";

	printf("\nISIS-1304 - FUNDAMENTOS DE INFRAESTRUCTURA TECNOLOGICA - PROYECTO 2\n");

	if (argc != 5)
	{
		printf("Se ingreso un numero incorrecto de argumentos (%d) o se ingresaron de forma erronea... :(\n", argc);
		system("pause");
		return -1;
	}

	strcat(nomArch, argv[1]); //Se concatena el nombre del archivo en su variable respectiva
	op = argv[2][0]; //Se extrae la opción a realizar
	sscanf(argv[3], "%d", &num); //Se formatea el número de bits por Byte para leer o insertar

	if (op == 'w') {
		strcat(msg, argv[4]); //Se concatena la cadena a escribir en su variable, si se insertará un mensaje
	}
	else if (op == 'r') {
		sscanf(argv[4], "%d", &l); //Se formatea en l la longitud a leer de caracteres, si se leerá un mensaje
	}
	else {
		printf("Se ingreso una opcion invalida para el programa... :(\n");
		system("pause");
		return -1;
	}

	printf("Se ingresaron los %d argumentos correctamente!\n", argc);

	// Cargar los datos
	cargarBMP24(img, nomArch);

	if (op == 'w') {
		printf("\nMODO INSERTAR MENSAJE\n\n");
		printf(" - Longitud mensaje: %d bytes\n", strlen(msg));
		//msg[strlen(msg)] = '\0';

		printf(" - Insertando mensaje . . .\n");
		insertarMensaje(img, msg, num);
		printf(" - Insertado correctamente!\n");

		strcat(nomArch, "generado.bmp");
		guardarBMP24(img, nomArch);

		printf(" - Mensaje insertado y guardado exitosamente en el archivo/ruta '%s'! :D\n\n", nomArch);
	}
	else if (op == 'r') {
		printf("\nMODO LEER MENSAJE\n\n");

		for (int i = 0; i < l; i++) {
			msg[i] = 0;
		}
		msg[l] = 0;

		leerMensaje(img, msg, l, num);

		printf(" - Mensaje obtenido exitosamente! El mensaje es:\n\n\t%s\n\n", msg);
	}

	system("pause");
	return 0;
}

/**
* Inserta un mensaje, de a n bits por componente de color, en la imagen apuntada por img
* parámetro img: Apuntador a una imagen en cuyos pixeles se almacenará el mensaje.
* parámetro mensaje: Apuntador a una cadena de caracteres con el mensaje.
* parámetro n: Cantidad de bits del mensaje que se almacenarán en cada componente de color de cada pixel. 0 < n <= 8.
*/
// DESARROLLAR EN ENSAMBLADOR, *NO* SE PUEDEN USAR NOMBRES SIMBOLICOS
void insertarMensaje(Imagen * img, unsigned char mensaje[], int n) 
{
	__asm {
		push ebp
		mov esp, ebp
		sub esp, 32; da el espacio para las variables tmp
		push ebx
		push ecx
		push edx
		mov eax, [ebp + 8]
		add eax, 8
		mov eax, [eax]; apuntador a informacion de la imagen
		mov[ebp - 12], eax; asigna a  ebp - 12 el apuntador de la informacion de la imagen
		mov eax, [ebp + 8]
		add eax, 4
		mov eax, [eax]
		mov[ebp - 8], eax; asigna a  ebp - 8 el alto de la imagen
		mov eax, [ebp + 8]
		mov eax, [eax]
		mov[ebp - 4], eax; asigna a ebp - 4 el ancho de la imagen
		mov eax, 0
		mov[ebp - 16], eax; inicializa counter en 0 (Cantidad de bits insertados)
		mov[ebp - 20], eax; inicializa index en 0 (index es el char en el que va)
		mov[ebp - 24], eax; lector
		mov ebx, 0; utiliza ebx para llevar la cuenta del ciclo
		forAlto :
		push ebx
			mov ebx, 0
			forAncho :
			mov eax, 255
			mov[ebp - 28], eax; inicializa helper en 255
			mov eax, [ebp + 12]
			add eax, [ebp - 20]; le suma el index a la posicion del vector
			cmp eax, 0; Si estoy en el caracter final
			je break

			add eax, 1; mensaje[index + 1]
			cmp eax, 0; Si estoy en el caracter final
			jne continuar
			mov edx, 0; revisa que la multiplicacion no altere edx
			mov eax, [ebp - 20]; en eax pone index
			inc eax
			push ecx
			mov ecx,8
			imul ecx ; multiplica index + 1 por 8
			pop ecx
			sub eax, [ebp - 16]; le resto counter
			cmp eax, [ebp + 16]; lo compara con n
			jge continuar
			mov eax, [ebp + 16]
			push eax; pasa n por parametro
			mov eax, [ebp - 16]
			push eax; pasa counter por parametro
			mov eax, [ebp + 12]
			push eax; pasa mensaje por parametro
			call sacarNbits; Llama la funcion
			add esp, 12; elimina los parametros enviados
			mov[ebp - 32], al; asigna a bitschar el resultado del call
			mov eax, [ebp - 20]
			inc eax
			push ecx
			push edx
			mov edx,0
			mov ecx, 8
			imul ecx
			pop edx
			pop ecx
			sub eax, [ebp - 16]
			mov edx, [ebp + 16]
			sub edx, eax; Operaciones para(n - (((index + 1) * 8) - counter))
			movzx ecx, dl; pasar de un numero de 32 a un numero de 8
			shl[ebp - 32], cl;
		mov edx, [ebp + 16]; corrimiento sobre helper con n
			movzx ecx, dl
			shl[ebp - 28], cl;  Corrimiento de n bits en helper
			mov eax, [ebp - 12]
			mov eax, [eax]; obtiene lo apuntado por image
			and eax, [ebp - 28]
			mov edx, [ebp - 12];
		mov[edx], al;  modifica lo apuntado por image.RECORDAR QUE SON CHARS
			mov edx, [edx]
			mov[ebp - 24], dl; lector recibe lo apuntado por image

			mov al, [ebp - 24]; le paso el char del lector a al
			or al, [ebp - 32]
			mov edx, [ebp - 12]
			mov[edx], al
			jmp break

			continuar:

		mov eax, [ebp + 16]
			push eax; pasa n por parametro
			mov eax, [ebp - 16]
			push eax; pasa counter por parametro
			mov eax, [ebp + 12]
			push eax; pasa mensaje por parametro
			call sacarNbits; Llama la funcion
			add esp, 12; elimina los parametros enviados
			mov[ebp - 32], al; asigna a bitschar el resultado del call

			mov edx, [ebp + 16]; corrimiento sobre helper con n
			movzx ecx, dl
			shl[ebp - 28], cl;  Corrimiento de n bits en helper
			mov eax, [ebp - 12]
			mov eax, [eax]; obtiene lo apuntado por image
			and eax, [ebp - 28]
			mov edx, [ebp - 12];
		mov[edx], al;  modifica lo apuntado por image.RECORDAR QUE SON CHARS
			mov edx, [edx]
			mov[ebp - 24], dl; lector recibe lo apuntado por image

			mov al, [ebp - 24]; le paso el char del lector a al
			or al, [ebp - 32]
			mov edx, [ebp - 12]
			mov[edx], al

			inc[ebp - 12]

			mov eax, [ebp - 16]
			add eax, [ebp + 16]
			mov [ebp - 16], eax; counter += n

			mov eax, [ebp - 16]
			push ecx
			push edx
			mov edx,0
			mov ecx, 8
			idiv ecx; divide por 8
			pop edx
			pop ecx
			mov[ebp - 20], eax; index = counter / 8

			inc ebx
			cmp ebx, [ebp - 4]
			jne forAncho


			pop ebx
			inc ebx
			cmp ebx, [ebp - 8]
			jne forAlto
			break:
			pop edx 
			pop ecx
			pop ebx
			add esp,32
			pop ebp 
		ret
	}
}

/**
* Extrae un mensaje de tamanio l, guardado de a n bits por componente de color, de la imagen apuntada por img
* parámetro img: Apuntador a una imagen que tiene almacenado el mensaje en sus pixeles.
* parámetro msg: Apuntador a una cadena de caracteres donde se depositará el mensaje.
* parámetro l: Tamanio en bytes del mensaje almacenado en la imagen.
* parámetro n: Cantidad de bits del mensaje que se almacenan en cada componente de color de cada pixel. 0 < n <= 8.
*/
// DESARROLLAR EN ENSAMBLADOR, SE PUEDEN USAR NOMBRES SIMBOLICOS
void leerMensaje(Imagen * img, unsigned char msg[], int l, int n) {
	__asm {

	}
}

/**
* Extrae n bits a partir del bit que se encuentra en la posición bitpos en la secuencia de bytes que
* se pasan como parámetro
* parámetro secuencia: Apuntador a una secuencia de bytes.
* parámetro n: Cantidad de bits que se desea extraer. 0 < n <= 8.
* parámetro bitpos: Posición del bit desde donde se extraerán los bits. 0 <= n < 8*longitud de la secuencia
* retorno: Los n bits solicitados almacenados en los bits menos significativos de un unsigned char
*/
// DESARROLLAR EN ENSAMBLADOR, SE PUEDEN USAR NOMBRES SIMBOLICOS
unsigned char sacarNbits(unsigned char secuencia[], int bitpos, int n)
 {
	// DESARROLLO OPCIONAL: Puede ser útil para el desarrollo de los procedimientos obligatorios.

	__asm {
		push ebp
		mov esp, ebp
		sub esp, 12
		push edx
		push ecx
		push ebx
		mov eax, 0
		mov[ebp - 4], eax
		mov[ebp - 8], eax
		mov eax, 255
		mov[ebp - 12], eax
		mov ecx, [ebp - 4]

		cmp[ebp + 12], 8
		jl intermedio
		mov eax, bitpos; index = bitpos / 8
		mov edx, 0
		mov ecx, 8
		idiv ecx
		mov[ebp - 4], eax; int toSubstract = index * 8
		mov eax, [ebp - 4]; se recupera lo de index - int toSubstract = index * 8
		mov edx, 0;
		imul ecx; se multiplica el index por ocho.
			mov edx, eax; se copia el resultado de la multiplicación de index * 8
			sub bitpos, edx; bitpos -= toSubstract
			intermedio :
		mov dl, [ebp - 12]; helper = helper >> bitpos
			mov eax, bitpos
			movzx ecx, al; Pasar un numero de 32 a uno de 8
			shr dl, cl;
		mov[ebp - 12], edx; se pega en la variable el resultado que se acaba de
			mov dl, [ebp - 12]; answer = helper & secuencia[index]
			mov ecx, [ebp - 4]; se mueve index a ecx
			mov ebx, secuencia
			add ebx, ecx
			and dl, [ebx]; se hace el and logico
			mov[ebp - 8], edx; se copia el valor obtenido a la variable answer
			mov edx, 8; 8 - bitpos < n && secuencia[index + 1] != '\0'
			sub edx, bitpos; se resta para hacer la primera comparación
			cmp edx, n; se hace la comparación con n
			jge tercerIf
			mov edx, [ebp - 4]; se mueve index
			inc edx; se incrementa en 1 edx
			cmp secuencia[edx], 0
			je tercerIf
			segundoIf :
		mov eax, 255;
		mov[ebp - 12], eax; helper = 255
			mov edx, 8; int shiftLeft = 8 - (n - (8 - bitpos))
			sub edx, bitpos; se hace la primera resta del parentesis
			mov eax, n; en eax va a quedar shiftleftans
			sub eax, edx; int shiftLeftans = n - (8 - bitpos)
			mov ecx, n; se guarda n
			sub ecx, edx; se hace n - (8 - bitpos)
			mov edx, 8; se pone 8 en edx
			sub edx, ecx; en edx va a quedar shiftleft
			movzx ecx, dl; pasar de un numero de 32 a un numero de 8
			shl[ebp - 12], cl; helper = helper << shiftLeft
			movzx ecx, al
			shl[ebp - 8], cl; answer = answer << shiftLeftans
			mov bl, [ebp - 12]; unsigned char temporary = helper & secuencia[index + 1]
			mov eax, [ebp - 4]; se guarda index
			inc eax; se incrementa eax
			mov ecx, secuencia
			add ecx, eax
			and bl, [ecx]; se hace el y logico que se guarda en dl
			movzx ecx, dl
			shr bl, cl; se corre hacia la derecha el dl temporary = temporary >> shiftLeft
			or [ebp - 8], bl; answer = answer | temporary;
		jmp fin
			tercerIf :
		mov eax, 8; se mueve a eax el 8
			sub eax, bitpos; se resta a 8 bitpos
			cmp eax, n; (8 - bitpos > n)
			jle fin
			mov eax, 8;
		mov ebx, n; se pasa a ebx la n
			add ebx, bitpos; se suma ebx con bitpos
			sub eax, ebx; 8 - (n + bitpos)
			movzx ecx, al
			shr[ebp - 8], cl; answer >> shiftRight
			fin :
		pop ebx
			pop ecx
			pop edx
			mov eax, 0
			movzx eax, [ebp - 8]; se pone la respuesta
			add esp, 12; se vacian las variables locales
			pop ebp
			ret
	}
}

// Lee un archivo en formato BMP y lo almacena en la estructura img
// NO MODIFICAR
void cargarBMP24(Imagen * imagen, char * nomArchivoEntrada) {
	// bmpDataOffset almacena la posición inicial de los datos de la imagen. Las otras almacenan el alto y el ancho
	// en pixeles respectivamente
	int bmpDataOffset, bmpHeight, bmpWidth;
	int y;
	int x;
	int	residuo;

	FILE *bitmapFile;
	bitmapFile = fopen(nomArchivoEntrada, "rb");
	if (bitmapFile == NULL) {
		printf("No ha sido posible cargar el archivo: %s\n", nomArchivoEntrada);
		exit(-1);
	}

	fseek(bitmapFile, 10, SEEK_SET); // 10 es la posición del campo "Bitmap Data Offset" del bmp	
	fread(&bmpDataOffset, sizeof(int), 1, bitmapFile);

	fseek(bitmapFile, 18, SEEK_SET); // 18 es la posición del campo "height" del bmp
	fread(&bmpWidth, sizeof(int), 1, bitmapFile);
	bmpWidth = bmpWidth * 3;

	fseek(bitmapFile, 22, SEEK_SET); // 22 es la posición del campo "width" del bmp
	fread(&bmpHeight, sizeof(int), 1, bitmapFile);

	residuo = (4 - (bmpWidth) % 4) & 3; // Se debe calcular los bits residuales del bmp, que surjen al almacenar en palabras de 32 bits

	imagen->ancho = bmpWidth;
	imagen->alto = bmpHeight;
	imagen->informacion = (unsigned char *)calloc(bmpWidth * bmpHeight, sizeof(unsigned char));

	fseek(bitmapFile, bmpDataOffset, SEEK_SET); // Se ubica el puntero del archivo al comienzo de los datos

	for (y = 0; y < bmpHeight; y++) {
		for (x = 0; x < bmpWidth; x++) {
			int pos = y * bmpWidth + x;
			fread(&imagen->informacion[pos], sizeof(unsigned char), 1, bitmapFile);
		}
		fseek(bitmapFile, residuo, SEEK_CUR); // Se omite el residuo en los datos
	}
	fclose(bitmapFile);
}

// Esta función se encarga de guardar una estructura de Imagen con formato de 24 bits (formato destino) en un archivo binario
// con formato BMP de Windows.
// NO MODIFICAR
void guardarBMP24(Imagen * imagen, char * nomArchivoSalida) {
	unsigned char bfType[2];
	unsigned int bfSize, bfReserved, bfOffBits, biSize, biWidth, biHeight, biCompression, biSizeImage, biXPelsPerMeter, biYPelsPerMeter, biClrUsed, biClrImportant;
	unsigned short biPlanes, biBitCount;
	FILE * archivoSalida;
	int y, x;
	int relleno = 0;

	int residuo = (4 - (imagen->ancho) % 4) & 3; // Se debe calcular los bits residuales del bmp, que quedan al forzar en palabras de 32 bits


	bfType[2];       // Tipo de Bitmap
	bfType[0] = 'B';
	bfType[1] = 'M';
	bfSize = 54 + imagen->alto * ((imagen->ancho) / 3) * sizeof(unsigned char);       // Tamanio total del archivo en bytes
	bfReserved = 0;   // Reservado para uso no especificados
	bfOffBits = 54;    // Tamanio total del encabezado
	biSize = 40;      // Tamanio del encabezado de informacion del bitmap	
	biWidth = (imagen->ancho) / 3;     // Ancho en pixeles del bitmap	
	biHeight = imagen->alto;    // Alto en pixeles del bitmap	
	biPlanes = 1;    // Numero de planos	
	biBitCount = 24;  // Bits por pixel (1,4,8,16,24 or 32)	
	biCompression = 0;   // Tipo de compresion
	biSizeImage = imagen->alto * imagen->ancho;   // Tamanio de la imagen (sin ecabezado) en bits
	biXPelsPerMeter = 2835; // Resolucion del display objetivo en coordenada x
	biYPelsPerMeter = 2835; // Resolucion del display objetivo en coordenada y
	biClrUsed = 0;       // Numero de colores usados (solo para bitmaps con paleta)	
	biClrImportant = 0;  // Numero de colores importantes (solo para bitmaps con paleta)	

	archivoSalida = fopen(nomArchivoSalida, "w+b"); // Archivo donde se va a escribir el bitmap
	if (archivoSalida == 0) {
		printf("No ha sido posible crear el archivo: %s\n", nomArchivoSalida);
		exit(-1);
	}

	fwrite(bfType, sizeof(char), 2, archivoSalida); // Se debe escribir todo el encabezado en el archivo. En total 54 bytes.
	fwrite(&bfSize, sizeof(int), 1, archivoSalida);
	fwrite(&bfReserved, sizeof(int), 1, archivoSalida);
	fwrite(&bfOffBits, sizeof(int), 1, archivoSalida);
	fwrite(&biSize, sizeof(int), 1, archivoSalida);
	fwrite(&biWidth, sizeof(int), 1, archivoSalida);
	fwrite(&biHeight, sizeof(int), 1, archivoSalida);
	fwrite(&biPlanes, sizeof(short), 1, archivoSalida);
	fwrite(&biBitCount, sizeof(short), 1, archivoSalida);
	fwrite(&biCompression, sizeof(int), 1, archivoSalida);
	fwrite(&biSizeImage, sizeof(int), 1, archivoSalida);
	fwrite(&biXPelsPerMeter, sizeof(int), 1, archivoSalida);
	fwrite(&biYPelsPerMeter, sizeof(int), 1, archivoSalida);
	fwrite(&biClrUsed, sizeof(int), 1, archivoSalida);
	fwrite(&biClrImportant, sizeof(int), 1, archivoSalida);

	// Se escriben en el archivo los datos RGB de la imagen.
	for (y = 0; y < imagen->alto; y++) {
		for (x = 0; x < imagen->ancho; x++) {
			int pos = y * imagen->ancho + x;
			fwrite(&imagen->informacion[pos], sizeof(unsigned char), 1, archivoSalida);
		}
		fwrite(&relleno, sizeof(unsigned char), residuo, archivoSalida);
	}
	fclose(archivoSalida);
}