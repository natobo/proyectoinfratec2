
#define _CRT_SECURE_NO_DEPRECATE 
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>








/**
* Muestra el numero binario de un unsigned char
*/
void showBinary(unsigned char a) {
	while (a) {
		if (a & 1)
			printf("1");
		else
			printf("0");
		a = a >> 1;
	}
	printf("\n");
}
unsigned char sacarNbits(unsigned char secuencia[], int bitpos, int n)
{
	// DESARROLLO OPCIONAL: Puede ser útil para el desarrollo de los procedimientos obligatorios.
	__asm {
		push ebp
		mov ebp, esp
		sub esp, 12
		push edx
		push ecx
		push ebx
		mov[ebp - 4], 0
		mov[ebp - 8], 0
		mov[ebp - 12], 255

		cmp[ebp + 8], 8
		jl intermedio
		mov eax, bitpos; index = bitpos / 8
		mov edx, 0
		idiv 8
		mov[ebp - 4], eax; int toSubstract = index * 8
		mov eax, [ebp - 4]; se recupera lo de index - int toSubstract = index * 8
		mov edx, 0;
		imul 8; se multiplica el index por ocho.
			mov edx, eax; se copia el resultado de la multiplicación de index * 8
			sub bitpos, edx; bitpos -= toSubstract
			intermedio :
		mov dl, [ebp - 12]; helper = helper >> bitpos
			shr dl, bitpos;
		mov[ebp - 12], dl; se pega en la variable el resultado que se acaba de
			mov dl, [ebp - 12]; answer = helper & secuencia[index]
			mov ecx, [ebp - 4]; se mueve index a ecx
			and dl, secuencia[ecx]; se hace el and logico
			mov[ebp - 8], dl; se copia el valor obtenido a la variable answer
			mov edx, 8; 8 - bitpos < n && secuencia[index + 1] != '\0'
			sub edx, bitpos; se resta para hacer la primera comparación
			cmp edx, n; se hace la comparación con n
			jge tercerIf
			mov edx, [ebp - 4]; se mueve index
			inc edx; se incrementa en 1 edx
			cmp secuencia[edx], 0
			unsigned char sacarNbits(unsigned char secuencia[], int bitpos, int n)
		{
			// DESARROLLO OPCIONAL: Puede ser útil para el desarrollo de los procedimientos obligatorios.
			__asm {
				push ebp
				mov ebp, esp
				sub esp, 12
				push edx
				push ecx
				push ebx
				mov[ebp - 4], 0
				mov[ebp - 8], 0
				mov[ebp - 12], 255

				cmp[ebp + 8], 8
				jl intermedio
				mov eax, bitpos; index = bitpos / 8
				mov edx, 0
				idiv 8
				mov[ebp - 4], eax; int toSubstract = index * 8
				mov eax, [ebp - 4]; se recupera lo de index - int toSubstract = index * 8
				mov edx, 0;
				imul 8; se multiplica el index por ocho.
					mov edx, eax; se copia el resultado de la multiplicación de index * 8
					sub bitpos, edx; bitpos -= toSubstract
					intermedio :
				mov dl, [ebp - 12]; helper = helper >> bitpos
					shr dl, bitpos;
				mov[ebp - 12], dl; se pega en la variable el resultado que se acaba de
					mov dl, [ebp - 12]; answer = helper & secuencia[index]
					mov ecx, [ebp - 4]; se mueve index a ecx
					and dl, secuencia[ecx]; se hace el and logico
					mov[ebp - 8], dl; se copia el valor obtenido a la variable answer
					mov edx, 8; 8 - bitpos < n && secuencia[index + 1] != '\0'
					sub edx, bitpos; se resta para hacer la primera comparación
					cmp edx, n; se hace la comparación con n
					jge tercerIf
					mov edx, [ebp - 4]; se mueve index
					inc edx; se incrementa en 1 edx
					cmp secuencia[edx], 0
					je tercerIf
					segundoIf :
				mov[ebp - 12], 255; helper = 255
					mov edx, 8; int shiftLeft = 8 - (n - (8 - bitpos))
					sub edx, bitpos; se hace la primera resta del parentesis
					mov eax, n; en eax va a quedar shiftleftans
					sub eax, edx; int shiftLeftans = n - (8 - bitpos)
					mov ecx, n; se guarda n
					sub ecx, edx; se hace n - (8 - bitpos)
					mov edx, 8; se pone 8 en edx
					sub edx, ecx; en edx va a quedar shiftleft
					shl[ebp - 12], edx; helper = helper << shiftLeft;
				shl[ebp - 8], eax; answer = answer << shiftLeftans;
				mov cl, [ebp - 12]; unsigned char temporary = helper & secuencia[index + 1]
					mov eax, [ebp - 4]; se guarda index
					inc eax; se incrementa eax
					and cl, secuencia[eax]; se hace el y logico que se guarda en dl
					shr cl, edx; se corre hacia la derecha el dl temporary = temporary >> shiftLeft
					or [ebp - 8], cl; answer = answer | temporary;
				je fin
					tercerIf :
				mov eax, 8; se mueve a eax el 8
					sub eax, bitpos; se resta a 8 bitpos
					cmp eax, n; (8 - bitpos > n)
					jle fin
					mov eax, 8;
				mov ebx, n; se pasa a ebx la n
					add ebx, bitpos; se suma ebx con bitpos
					sub eax, ebx; 8 - (n + bitpos)
					shr[ebp - 8], eax; answer >> shiftRight
					fin :
				pop ebx
					pop ecx
					pop edx
					mov eax, 0
					add esp, 12; se vacian las variables locales
					mov al, [ebp - 8]; se pone la respuesta
					ret
			}
		}	je tercerIf
			segundoIf :
		mov[ebp - 12], 255; helper = 255
			mov edx, 8; int shiftLeft = 8 - (n - (8 - bitpos))
			sub edx, bitpos; se hace la primera resta del parentesis
			mov eax, n; en eax va a quedar shiftleftans
			sub eax, edx; int shiftLeftans = n - (8 - bitpos)
			mov ecx, n; se guarda n
			sub ecx, edx; se hace n - (8 - bitpos)
			mov edx, 8; se pone 8 en edx
			sub edx, ecx; en edx va a quedar shiftleft
			shl[ebp - 12], edx; helper = helper << shiftLeft;
		shl[ebp - 8], eax; answer = answer << shiftLeftans;
		mov cl, [ebp - 12]; unsigned char temporary = helper & secuencia[index + 1]
			mov eax, [ebp - 4]; se guarda index
			inc eax; se incrementa eax
			and cl, secuencia[eax]; se hace el y logico que se guarda en dl
			shr cl, edx; se corre hacia la derecha el dl temporary = temporary >> shiftLeft
			or [ebp - 8], cl; answer = answer | temporary;
		je fin
			tercerIf :
		mov eax, 8; se mueve a eax el 8
			sub eax, bitpos; se resta a 8 bitpos
			cmp eax, n; (8 - bitpos > n)
			jle fin
			mov eax, 8;
		mov ebx, n; se pasa a ebx la n
			add ebx, bitpos; se suma ebx con bitpos
			sub eax, ebx; 8 - (n + bitpos)
			shr[ebp - 8], eax; answer >> shiftRight
			fin :
		pop ebx
			pop ecx
			pop edx
			mov eax, 0
			add esp, 12; se vacian las variables locales
			mov al, [ebp - 8]; se pone la respuesta
			ret
	}
}

int main() {
	char a[] = { 1,0 };
	
}